<?php

namespace Services;

use Exception;
use PDO;
use PDOException;

final class DB
{
    private string $host = '127.0.0.1';
    private string $dbname = 'boxberry';
    private string $user = 'root';
    private string $password = 'root';
    private PDO $pdo;
    private static DB $instance;

    /**
     * Иницилизируем базу
     */
    private function __construct()
    {
        try {
            $this->pdo = new PDO(
                "mysql:host={$this->host}dbname=$this->dbname",
                $this->user,
                $this->password
            );
            $this->pdo->exec('SET NAMES UTF8');
        } catch (PDOException $e) {
            echo 'Ошибка при подключении к БД: ' . $e->getMessage();
        }

    }

    /**
     * Подготовка и выполнение запроса SQL
     * @param string $sql
     * @param array $params
     * @return array|null
     * @throws Exception
     */
    public function query(string $sql, array $params = []): ?array
    {
        $sth = $this->pdo->prepare($sql);
        $result = $sth->execute($params);
        if (false === $result) {
            throw new Exception(implode('; ', $sth->errorInfo()));
        }

        return $sth->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * @return static
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}