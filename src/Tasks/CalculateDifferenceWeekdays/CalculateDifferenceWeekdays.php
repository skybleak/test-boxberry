<?php


try {
    if (isset($_GET['start']) && isset($_GET['end'])) {
        echo countWeekdays($_GET['start'], $_GET['end']);
    } else {
        throw new Exception('Не переданы даты');
    }
} catch (Exception $e) {
    echo 'Ошибка: ' . $e->getMessage();
}

/**
 * @param string $startDate
 * @param string $endDate
 * @param string $weekday
 * @return int
 * @throws Exception
 */
function countWeekdays(string $startDate, string $endDate, string $weekday = 'Tuesday'): int
{
    $startDate = date_create($startDate);
    $endDate = date_create("$endDate 23:59:59");
    if ($startDate > $endDate) {
        throw new Exception('Конечная дата не может быть раньше начальной');
    }

    $closestWeekday = $startDate->modify($weekday);

    $diffInDays = $endDate->diff($closestWeekday)->days;

    return (int)($diffInDays / 7) + 1;
}